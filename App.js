import React, {Component} from 'react';
import {View, Text} from 'react-native';

import OurtFlatlist from './app/components/ourFlatList/OurFlatList';
import ConexionFetch from './app/components/conexionFetch/ConexionFetch';


class App extends Component {
  constructor(props) {
    super(props);
    this.state = {};
  }

  render() {
    return (
      <View style={{flex: 1}}>
        <ConexionFetch/>
      </View>
    );
  }
}

export default App;
